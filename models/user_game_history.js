'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user_game_history.belongsTo(models.user_game, {
        foreignKey: 'user_game_id'
      });
      user_game_history.belongsTo(models.user_game_biodata, {
        foreignKey: 'user_game_biodata_id'
      });
    }
  }
  user_game_history.init({
    user_game_id: DataTypes.INTEGER,
    user_game_biodata_id: DataTypes.INTEGER,
    date_of_match: DataTypes.STRING,
    match_result: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user_game_history',
  });
  return user_game_history;
};